struct Node {
    int data;
    Node* next=nullptr;
};
class List {
    Node* first=nullptr;
public:
    auto insert(int value) -> void;
    auto print() const -> void;
    auto remove(int value) -> bool;
    auto search(int value) const ->void;
    auto change(int position,int value) -> bool;
    auto sort() -> void;
    auto empty() const -> bool;
    ~List();
};