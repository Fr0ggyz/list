#include <iostream>
#include <cstring>
#include <locale>
#include "List.hpp"


void showmenu();
void error();
int main(int argc, char* argv[]) {
    setlocale(LC_ALL, "RUS");
    List list;
    bool flag=false;
    if(argc == 1)
        flag=true;
    if(argc == 2 ) {
        char* first_value = argv[1] ;
        while(1) {
            list.insert(atoi(first_value));
            first_value = strchr(first_value, ',') + 1;
            if(!strchr(first_value, ','))
                break;
        }
        list.insert(atoi(first_value));
    } else if(argc >2 ) {
        for (int i = 1; i < argc ; i++) {
            list.insert(atoi(argv[i]));
        }
    }
    for(;;) {
LOOP:
        showmenu();
        int count;
        std::cin >> count;
        while (!std::cin) {
            error();
            std::cout<< "\033[0;32mПовторите ввод: \033[0;34m";
            std::cin >> count;
        }

        if(!list.empty() && count !=2 && count != 7) count=0;
        switch(count) {
        case 0:
            std::cout<<"\033[1;31mСписок пуст!\033[0;34m"<<std::endl;
            break;

        case 1:
            list.print();
            break;

        case 2: {
            std::cout<<"\033[0;32mВведите значение(-я):\033[0;34m";
            int value;
            std::cin>>value;
            if(std::cin) {
                list.insert(value);
                while(std::cin.get() != '\n') {
                    if(!(std::cin >> value)) {
                        error();
                        std::cout<< "\033[0;32mПродолжите ввод: \033[0;34m";
                        std::cin>>value;
                    }
                    list.insert(value);
                }
            } else
                error();
        }
        break;

        case 3: {
            std::cout<<"\033[0;32mВведите значение(-я):\033[0;34m";
            bool f;
            int value;
            std::cin>>value;
            while(std::cin) {
                f = list.remove(value);
                if(!f) {
                    std::cout<<"\033[1;31mЗначение "<<value<<" не найдено!\033[0;34m"<<std::endl;
                }
                if(std::cin.get()=='\n') {
                    goto LOOP;
                }
                std::cin>>value;
            }
            error();
        }
        break;

        case 4: {
            std::cout<<"\033[0;32mВведите значение:\033[0;34m";
            int value;
            std::cin>>value;
            if(std::cin)
                list.search(value);
            else
                error();
        }
        break;

        case 5: {
            std::cout<<"\033[0;32mВведите позицию и значение элемента: \033[0;34m";
            int position;
            std::cin>>position;
            int value;
            std::cin>>value;
            if(!std::cin) {
                error();
                break;
            }
            bool f = list.change(position, value);
            if(!f)
                std::cout<<"\033[1;31mЭлемент с позицией "<<position<<" не существует!\033[0;34m"<<std::endl;
        }
        break;

        case 6:
            list.sort();
            break;

        case 7: {
            std::string  Answer;
            std::cout <<"\033[1;31mВы хотите выйти из программы ?(y/n)\033[0;34m " << std::endl;
            std::cin >> Answer;
            if ((Answer == "y") || (Answer == "Y") || (Answer == "YES") || (Answer == "yes") || (Answer == "Yes")) {
                std::cout <<"\033[1;32mДо свидания!"<< std::endl;
                exit(0);
            }else if ((Answer == "n") || (Answer == "N") || (Answer == "NO") || (Answer == "no") || (Answer == "No"))
               break;
            else 
               error();
        }
        break;
        }
    }
    return 0;
}